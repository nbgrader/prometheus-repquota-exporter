# Prometheus Repquota Exporter

This prometheus exporter exports information about disk quotas on a linux system, as reported by the `repquota` command.
For each user:
- disk space used
- soft limit
- hard limit

## Settings
(edit at the top of prometheus-repquota-exporter.py)
- Uses port 10018 by default
- Reports on the file system at `/home` by default

## Installation (Debian)

- clone this repository to `/usr/local/lib`
- change the group ownership of the `prometheus-repquota-exporter.py` file in it to prometheus, by running `sudo chown root:prometheus /usr/local/lib/prometheus-repquota-exporter/prometheus-repquota-exporter.py`
- give execution permision on the `prometheus-repquota-exporter.py` file, by running `sudo chmod u=rx,g=rx,o=r /usr/local/lib/prometheus-repquota-exporter/prometheus-repquota-exporter.py`
- give sudo permission for the prometheus user to use the `repquota` command:
	- create a `prometheus` file in `/etc/sudoers.d/` and write `prometheus ALL=(root) NOPASSWD: /usr/sbin/repquota` in it by running: `sudo sh -c 'echo "prometheus ALL=(root) NOPASSWD: /usr/sbin/repquota" >> /etc/sudoers.d/prometheus'`
	- give it the right permissions with `sudo chmod u=r,g=r,o= /etc/sudoers.d/prometheus`
- configure systemd to run this exporter and keep it running:
	- copy the `prometheus-repquota-exporter.service` file from this repository to `/lib/systemd/system/` by running: `sudo cp /usr/local/lib/prometheus-repquota-exporter/prometheus-repquota-exporter.service /lib/systemd/system/`
	- enable the service to ensure the service starts when the system boots: `sudo systemctl enable prometheus-repquota-exporter.service`
	- start the service with `sudo systemctl start prometheus-repquota-exporter.service`
	- check that the service is running, with `sudo systemctl status prometheus-repquota-exporter.service`
- you can check that you are indeed getting metrics exported by using `curl http://0.0.0.0:10018/metrics`, which should output lines like this:
```
repquota_disk_space_hard_limit{user="p2317605"} 6.291456e+08
repquota_disk_space_hard_limit{user="p1931591"} 6.291456e+08
repquota_disk_space_hard_limit{user="p2319504"} 6.291456e+08
```

## Notes

If the machine running this exporter has a firewall, make sure your prometheus scraper is allowed to access it on the port the exporter is running on. If the firewall is nftable, you can edit the `/etc/firewall/fragments.d/filter/input.nft` file, and add a line like this to the `chain input` function:
```
ip saddr IPOFYOURPROMETHEUSSCRAPER tcp dport 10018 ct state new counter accept
```
Then restart nftable with:
```
sudo systemctl restart nftables.service
```