#!/usr/bin/env python3

from prometheus_client import start_http_server, Gauge
from prometheus_client import REGISTRY, PROCESS_COLLECTOR, PLATFORM_COLLECTOR, GC_COLLECTOR
from prometheus_client.core import GaugeMetricFamily
import time
import re
import subprocess

##### Configuration (start)

PATHTOFILESYSTEM = "/home"
PORTNUMBER = 10018

##### Configuration (end)

# Removing useless default metrics about python's runtime and garbage collector
REGISTRY.unregister(PROCESS_COLLECTOR)
REGISTRY.unregister(PLATFORM_COLLECTOR)
REGISTRY.unregister(GC_COLLECTOR)

def collectRepquota():
    repquota = subprocess.run(['sudo', 'repquota', '-C',PATHTOFILESYSTEM], stdout=subprocess.PIPE)
    rawQuotas = repquota.stdout.decode('utf8') # String containing repquota's output
    """ Format of quotas:
    [
        {
            'username':string,
            'ratio':float, (=space_used/space_softlimit)
            'space_used':int(Byte),
            'space_softlimit':int(Byte),
            'space_hardlimit':int(Byte)
        }
    ]
    """
    quotas = []
    for l in rawQuotas.strip().splitlines()[5:]: # Enumerate all lines, skipping the first 5 (the repquota header)
        split = re.split(r' +', l.strip())
        # print(f"split= ", split)
        # Example output of that print: split=  ['root', '--', '5268', '614400', '614400', '1243', '0', '0']
        # repquota says it reports in KiloByte, but when the quota is 600M, and it reports 600*1024kB, so that's actually kiB
        record = {
            'username':split[0],
            'space_used':int(split[2])*1024,
            'space_softlimit':int(split[3])*1024,
            'space_hardlimit':int(split[4])*1024,
        }
        quotas.append(record)
    repquota_disk_space_used_metric = GaugeMetricFamily('repquota_disk_space_used', 'Quota information about user disk usage, obtained from repquota', labels=['user'])
    repquota_disk_space_soft_limit_metric = GaugeMetricFamily('repquota_disk_space_soft_limit', 'Soft limit for disk space used', labels=['user'])
    repquota_disk_space_hard_limit_metric = GaugeMetricFamily('repquota_disk_space_hard_limit', 'Hard limit for disk space used', labels=['user'])
    for record in quotas:
        repquota_disk_space_used_metric.add_metric([record['username']], record['space_used'])
        repquota_disk_space_soft_limit_metric.add_metric([record['username']], record['space_softlimit'])
        repquota_disk_space_hard_limit_metric.add_metric([record['username']], record['space_hardlimit'])
    return [
        repquota_disk_space_used_metric,
        repquota_disk_space_soft_limit_metric,
        repquota_disk_space_hard_limit_metric
    ]

class RepquotaCollector():
    def collect(self):
        metrics = collectRepquota()
        for m in metrics:
            yield m

if __name__ == '__main__':
    print("starting server on port {}".format(PORTNUMBER))
    # Start up the server to expose the metrics.
    start_http_server(PORTNUMBER)
    REGISTRY.register(RepquotaCollector())
    while True:
        time.sleep(1)